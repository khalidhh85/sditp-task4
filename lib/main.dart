import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:sditp_team1_app/pages/Controller/ProductProvider.dart';
import 'package:sditp_team1_app/pages/home_page.dart';
import 'package:sditp_team1_app/pages/login_page.dart';
import 'Repository/auth_repository.dart';




Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(App());
}

class App extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
  return MultiProvider(
  providers: [
  Provider<AuthenticationService>(
  create: (_) => AuthenticationService(FirebaseAuth.instance),
  ),
  StreamProvider(
  create: (context) => context.read<AuthenticationService>().authStateChanges,
  ),
  ChangeNotifierProvider.value(value: ProductProvider()),
  ],
  child: MaterialApp(
  title: 'Store Owner',
  theme: ThemeData(
  primarySwatch: Colors.blue,
  visualDensity: VisualDensity.adaptivePlatformDensity,
  ),
  home: AuthenticationWrapper(),
  ),
  );
  }
  }

  class AuthenticationWrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
  final firebaseUser = context.watch<User>();

  if (firebaseUser != null) {
  return HomePage();
  }
  return LoginPage();
  }
  }
