import 'package:flutter/material.dart';

import 'ProductCard.dart';

class StoreProducts extends StatefulWidget {
  @override
  _StoreProductsState createState() => _StoreProductsState();
}

class _StoreProductsState extends State<StoreProducts> {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 230,
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: 4,
            itemBuilder: (_, index) {
              return ProductCard(
                name: 'Winter Blazer',
                price: 50.99,
                picture: '',
              );
            }));
  }
}