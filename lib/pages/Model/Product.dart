import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:core';
class Product {
  String name;
  String description;
  String price;
  String imagePath;
  String productCategory;
  double productQuantity;

  //method to get the data from Cloud Database
  Product.fromSnapshot(DocumentSnapshot snap){
    Map productData = snap.data();
    name = productData["name"];
    description = productData["description"];
    price = productData["price"];
    imagePath = productData["imagePath"];
    productCategory = productData["productCategory"];
    productQuantity = productData["productQuantity"];
  }

}