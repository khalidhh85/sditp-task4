

import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import 'package:sditp_team1_app/Repository/auth_repository.dart';
import 'package:sditp_team1_app/pages/Controller/ProductProvider.dart';


class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ProductProvider productProvider = Provider.of<ProductProvider>(context);
    final _signOut = context.read<AuthenticationService>();
    return Scaffold(
      body: SafeArea(
        child: ListView(
          children: <Widget>[
//            App bar
            Stack(
              children: <Widget>[
                Positioned(
                    top: 0,
                    right: 50,
                    child: RaisedButton(
                      onPressed: () {
                        _signOut.signOut();
                      },
                      child: Text("Sign out"),
                    )
                ),
                Positioned(
                  top: 10,
                  right: 10,
                  child: Align(
                      alignment: Alignment.topRight,
                      child: Icon(Icons.person)),
                ),

                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text('Products are \nListed Below', style: TextStyle(fontSize: 30, color: Colors.black.withOpacity(0.6), fontWeight: FontWeight.w400),),
                ),
              ],
            ),

//            featured products
            Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(14.0),
                  child: Container(
                      alignment: Alignment.centerLeft,
                      child: new Text('Products')),
                ),
              ],
            ),

            Text(productProvider.products.length.toString(), style: TextStyle(color: Colors.black),),
//          recent products
            Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(14.0),
                  child: Container(
                      alignment: Alignment.centerLeft,
                      child: new Text('Recent products')),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}