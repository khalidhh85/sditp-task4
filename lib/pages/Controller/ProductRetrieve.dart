import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:async';
import 'package:sditp_team1_app/pages/Model/Product.dart';
class ProductRetrieve{
  FirebaseFirestore _firestore = FirebaseFirestore.instance;
  Future<List<Product>> getProducts() =>
      _firestore.collection('Products').get().then((snap){ // Used to get the products from firebase
          List<Product> products = []; //Array to store the products in this app
          snap.docs.map((snapshot) => products.add(Product.fromSnapshot(snapshot))); //Creating instances of Products for each product in database
          return products;
});

}