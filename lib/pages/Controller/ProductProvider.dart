import 'package:flutter/cupertino.dart';
import 'package:sditp_team1_app/pages/Model/Product.dart';

import 'ProductRetrieve.dart';

class ProductProvider with ChangeNotifier{
  List<Product> _products = []; // To contain the products
  ProductRetrieve _productsRetrieve = ProductRetrieve(); //to pass the products to the main class

  ProductProvider() { // costructor
    _getProducts();
  }

//  getter
  List<Product> get products => _products;

//  methods
  void _getProducts() async { //fill the products array with data from the retrieval class
    _products = await _productsRetrieve.getProducts();
    notifyListeners();
  }
}